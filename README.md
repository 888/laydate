
## 概要
全面重写的 layDate 包含了大量的更新，其中主要以：年选择器、年月选择器、日期选择器、时间选择器、日期时间选择器 五种类型的选择方式为基本核心，并且均支持范围选择（即双控件）。内置强劲的自定义日期格式解析和合法校正机制，含中文版和国际版，主题简约却又不失灵活多样。由于内部采用的是零依赖的原生 JavaScript 编写，因此又可作为独立组件使用。毫无疑问，这是 layui 的虔心之作。

更新内容:
* 1.增加配置参数submit，值可以为true或false，意思值为true时，在对应控件单击或双击时自动提交数据，不用再点击确定按钮。
* 2.增加datetime模式时，可以双击提交数据，submit值须为true。
* 3.增加在月份选择时，可以单击选中月份后，直接提交数据，submit值须为true。


## 官网
[http://www.layui.com/laydate/](http://www.layui.com/laydate/)

## 相关
[文档](http://www.layui.com/doc/modules/laydate.html)、[社区](http://fly.layui.com)
